package com.myplanet.alexa.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.dotcms.repackage.javax.ws.rs.DELETE;
import com.dotcms.repackage.javax.ws.rs.POST;
import com.dotcms.repackage.javax.ws.rs.Path;
import com.dotcms.repackage.javax.ws.rs.PathParam;
import com.dotcms.repackage.javax.ws.rs.core.CacheControl;
import com.dotcms.repackage.javax.ws.rs.core.Context;
import com.dotcms.repackage.javax.ws.rs.core.Response;
import com.dotcms.repackage.javax.ws.rs.core.Response.ResponseBuilder;
import com.dotcms.rest.WebResource;
import com.dotmarketing.business.DotStateException;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myplanet.alexa.rest.models.DialogFlowEventInput;
import com.myplanet.alexa.rest.models.DialogFlowOutputContext;
import com.myplanet.alexa.rest.models.DialogFlowRequest;
import com.myplanet.alexa.rest.models.DialogFlowResponse;
import com.myplanet.alexa.rest.models.Movie;
import com.myplanet.alexa.rest.MovieService;

@Path("/dialogflow")
public class AlexaEndpoint  {

	private final WebResource webResource = new WebResource();
	private MovieService movieService = new MovieService();

	/**
	 * This is an authenticated rest service.
	 * 
	 * @param request
	 * @param params
	 * @return
	 * @throws DotStateException
	 * @throws DotDataException
	 * @throws DotSecurityException
	 * @throws IOException
	 */
	@POST
	@Path("/movie")
	public Response doPost(@Context HttpServletRequest request) {
		DialogFlowResponse response = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try{
			DialogFlowRequest req = objectMapper.readValue(request.getInputStream(),DialogFlowRequest.class);

			switch(req.getQueryResult().getIntent().getDisplayName()){
				case "ImportMovie":
					response = findAndImport(req);
					break;
				case "ImportMovie - select":
					response = importSelected(req);
					break;
				default:
					response = new DialogFlowResponse();
					response.setFulfillmentText("I did not understand. Please repeat");
					break;
			}

		}catch(Exception e){
			response.setFulfillmentText("Sorry an error has ocurred while importing the movie.");
		}
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);

		ResponseBuilder builder;
		try {
			objectMapper.setSerializationInclusion(Include.NON_NULL);
			builder = Response.ok(objectMapper.writeValueAsString(response), "application/json");
			return builder.cacheControl(cc).build();
		} catch (JsonProcessingException e) {
			builder = Response.serverError();
			return builder.cacheControl(cc).build();
		}

	}

	private DialogFlowResponse importSelected(DialogFlowRequest req) throws Exception {
		DialogFlowResponse response = new DialogFlowResponse();
		String strIndex = String.valueOf(req.getQueryResult().getParameters().get("number"));
		if("".equals(strIndex)){
			strIndex = String.valueOf(req.getQueryResult().getParameters().get("ordinal"));
		}
		Integer index = Double.valueOf(strIndex).intValue();
		List<String> idList = (List<String>) req.getQueryResult().getOutputContexts().get(0).getParameters().get("idList");
		List<String> titleList = (List<String>) req.getQueryResult().getOutputContexts().get(0).getParameters().get("titleList");
		movieService.importSingleMovie(idList.get(index-1));
		response.setFulfillmentText("Movie '"+titleList.get(index-1)+"' Imported");
		return response;
	}

	private DialogFlowResponse findAndImport(DialogFlowRequest req) throws Exception{
		DialogFlowResponse response = new DialogFlowResponse();
		String queryStr = (String) req.getQueryResult().getParameters().get("Movie");
		List<Movie> movieList = movieService.listMovies(queryStr);
		if(movieList == null || movieList.size() == 0){
			response.setFulfillmentText("No movie found for: "+queryStr);
		}else if(movieList.size() == 1){
			Movie movie = movieList.get(0);
			movieService.importSingleMovie(movie.getImdbid());
			response.setFulfillmentText("Movie '"+movie.getTitle()+" imported.");
		}else{
			StringBuilder sb = new StringBuilder();
			List<String> titleList = new ArrayList<String>();
			List<String> idList = new ArrayList<String>();
			DialogFlowOutputContext context = req.getQueryResult().getOutputContexts().get(0);
			sb.append("More than one movie found. Which one do you want to import?");
			for(int i=0; i<movieList.size() && i<5; i++){
				sb.append(movieList.get(i).getTitle());
				sb.append(".   ");
				titleList.add(movieList.get(i).getTitle());
				idList.add(movieList.get(i).getImdbid());
			}
			context.getParameters().put("titleList",titleList);
			context.getParameters().put("idList",idList);
			response.setOutputContexts(new ArrayList<DialogFlowOutputContext>());
			response.getOutputContexts().add(context);
			response.setFulfillmentText(sb.toString());
		}
		return response;
	}

	@DELETE
	@Path("/movie/{inode}")
	public Response doDelete(@Context HttpServletRequest request,@PathParam("inode") String inode){
		movieService.deleteAll(inode);
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		ResponseBuilder builder = Response.ok("OK", "text/plain");
		return builder.cacheControl(cc).build();
	}

}