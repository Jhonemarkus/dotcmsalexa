package com.myplanet.alexa.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myplanet.alexa.rest.models.Movie;
import com.myplanet.alexa.rest.models.OMDBResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.dotcms.contenttype.business.ContentTypeAPI;
import com.dotcms.contenttype.model.field.ImmutableDateField;
import com.dotcms.contenttype.model.field.ImmutableTextAreaField;
import com.dotcms.contenttype.model.field.ImmutableTextField;
import com.dotcms.contenttype.model.field.DataTypes;
import com.dotcms.contenttype.model.field.Field;
import com.dotcms.contenttype.model.type.ContentType;
import com.dotcms.contenttype.model.type.ImmutableSimpleContentType;
import com.dotmarketing.business.APILocator;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.portlets.contentlet.business.ContentletAPI;
import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.fasterxml.jackson.core.type.TypeReference;

public class MovieService {

    private String contentTypeIdentifier;

    public Movie getMovieDB(String title){
        Movie movie = null;
        String luceneQuery = "+contentType:Movie +languageId:1 +deleted:false +working:true +catchall:"+title+"*";
        try{
            List<Contentlet> movieList = APILocator.getContentletAPI().search(luceneQuery, 0, 0, null, APILocator.systemUser(), false);
            if(movieList != null && movieList.size()>0){
                Contentlet cont = movieList.get(0);
                movie = new Movie();
                movie.setTitle(cont.getStringProperty("title"));
                movie.setPlot(cont.getStringProperty("plot"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return movie;
    }

    public List<Movie> listMoviesDB(){
        List<Movie> movieList = new ArrayList<Movie>();
        try {
            createContentType();
            List<Contentlet> allMovies = APILocator.getContentletAPI().findByStructure(this.contentTypeIdentifier, APILocator.systemUser(), false, 10, 0);
            for(Contentlet cont:allMovies){
                Movie movie = new Movie();
                movie.setTitle(cont.getStringProperty("title"));
                movieList.add(movie);
            }
        } catch (DotDataException | DotSecurityException e) {
            e.printStackTrace();
        }
        return movieList;
    }

    public void createContentType() throws DotDataException, DotSecurityException{
        //Check if content type already exists
        ContentTypeAPI ctApi = APILocator.getContentTypeAPI(APILocator.systemUser());
        List<ContentType> list = ctApi.search("Movie");
        for(ContentType type : list){
            if(type.name().equals("Movie")){
                this.contentTypeIdentifier = type.id();
                break;
            }
        }
        //Create the content type if it does not exist
        if(this.contentTypeIdentifier == null){
            ImmutableSimpleContentType.Builder builder = ImmutableSimpleContentType.builder();
            ContentType movie = builder.name("Movie").folder(APILocator.systemHost().getFolder()).build();
            movie = ctApi.save(movie);
            
            List<Field> fieldList = new ArrayList<>();
            ImmutableTextField imdbid = ImmutableTextField.builder().name("IMDB Id").variable("imdbid").required(true).unique(true).contentTypeId(movie.id()).build();
            ImmutableTextField title = ImmutableTextField.builder().name("Title").indexed(true).required(true).contentTypeId(movie.id()).listed(true).build();
            
            ImmutableDateField releaseDate = ImmutableDateField.builder().name("Release Date").variable("releaseDate").contentTypeId(movie.id()).build();
            ImmutableTextField poster = ImmutableTextField.builder().name("Poster").contentTypeId(movie.id()).build();
            ImmutableTextField runtime = ImmutableTextField.builder().name("Runtime").contentTypeId(movie.id()).build();
            ImmutableTextAreaField plot = ImmutableTextAreaField.builder().name("Plot").contentTypeId(movie.id()).build();
            ImmutableTextField boxOffice = ImmutableTextField.builder().name("Box Office").variable("boxOffice").contentTypeId(movie.id()).build();
            fieldList.add(title);
            fieldList.add(releaseDate);
            fieldList.add(poster);
            fieldList.add(runtime);
            fieldList.add(plot);
            fieldList.add(boxOffice);
            fieldList.add(imdbid);

            ctApi.save(movie,fieldList);
            this.contentTypeIdentifier = movie.id();
        }
    }
    
    public List<Movie> listMovies(String search){
        try{
            search = search.replaceAll("\\s", "%20");
            String fullUrl = "http://www.omdbapi.com/?apikey=fb13b85d&s="+search+"&type=movie";
            
            URL obj = new URL(fullUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    
            // optional default is GET
            con.setRequestMethod("GET");
    
            //add request header
            //con.setRequestProperty("User-Agent", USER_AGENT);
    
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + fullUrl);
            System.out.println("Response Code : " + responseCode);
    
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
    
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
    
            //print result
            System.out.println("Reponse body:"+response.toString());
            //return response.toString();
            
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            OMDBResponse oResponse = objectMapper.readValue(response.toString(), OMDBResponse.class);
            
            //Check if result is OK
            if(oResponse.isResponse()) {
                return oResponse.getSearch();
            }else {
                return null;
            }
        }catch(IOException e){
            return null;
        }
    }

    public void importSingleMovie(String id) throws Exception {
        this.createContentType();
        String fullUrl = "http://www.omdbapi.com/?apikey=fb13b85d&i="+id;
        URL obj = new URL(fullUrl);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        //con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + fullUrl);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println("Reponse body:"+response.toString());
        //return response.toString();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        Movie movie = objectMapper.readValue(response.toString(), new TypeReference<Movie>() {});
        addMovie(movie);
    }

    public void addMovie(Movie movie) throws Exception {
        Contentlet m2 = new Contentlet();
        m2.setContentTypeId(this.contentTypeIdentifier);
        m2.setProperty("title", movie.getTitle());

        SimpleDateFormat releaseDateFormat = new SimpleDateFormat("dd MMM yyyy");
        Date date = releaseDateFormat.parse(movie.getReleased());
        m2.setProperty("releaseDate", date);

        m2.setProperty("imdbid", movie.getImdbid());
        m2.setProperty("poster", movie.getPoster());
        m2.setProperty("runtime", movie.getRuntime());
        m2.setProperty("plot", movie.getPlot());
        m2.setProperty("boxOffice", movie.getBoxoffice());
        m2.setLanguageId(1l);
        m2.setModDate(new Date());
        m2.setOwner("dotcms.org.1");
        ContentletAPI cApi = APILocator.getContentletAPIImpl();
        Contentlet checkedIn = cApi.checkin(m2, APILocator.systemUser(), false);
        cApi.publish(checkedIn, APILocator.systemUser(), false);
    }

    public void deleteAll(String inode){
        try{
            //String inode = "ddf29c1e-babd-40a8-bfed-920fc9b8c77f";
            List<Contentlet> allMovies = APILocator.getContentletAPI().findByStructure(inode, APILocator.systemUser(), false, 100, 0);
            APILocator.getContentletAPI().delete(allMovies, APILocator.systemUser(), false,true);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}