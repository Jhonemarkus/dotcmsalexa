package com.myplanet.alexa.rest;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.dotcms.repackage.javax.ws.rs.POST;
import com.dotcms.repackage.javax.ws.rs.Path;
import com.dotcms.repackage.javax.ws.rs.core.CacheControl;
import com.dotcms.repackage.javax.ws.rs.core.Context;
import com.dotcms.repackage.javax.ws.rs.core.Response;
import com.dotcms.repackage.javax.ws.rs.core.Response.ResponseBuilder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myplanet.alexa.rest.models.Movie;
import com.myplanet.alexa.rest.models.alexa.AlexaIntent;
import com.myplanet.alexa.rest.models.alexa.AlexaIntentRequest;
import com.myplanet.alexa.rest.models.alexa.AlexaIntentResponse;
import com.myplanet.alexa.rest.models.alexa.AlexaOutputSpeech;
import com.myplanet.alexa.rest.models.alexa.AlexaReprompt;
import com.myplanet.alexa.rest.models.alexa.AlexaResponse;

@Path("/alexa")
public class ReadMovieEndpoint{

    MovieService movieService = new MovieService();

    @POST
    @Path("/list")
    public Response listMovies(@Context HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        AlexaIntentResponse response = new AlexaIntentResponse();
        try {
            AlexaIntentRequest intentRequest = objectMapper.readValue(request.getInputStream(), AlexaIntentRequest.class);
            if(intentRequest != null && intentRequest.getRequest() != null && intentRequest.getRequest().getIntent() != null){
                AlexaIntent intent = intentRequest.getRequest().getIntent();
                switch(intent.getName()){
                    case "list_movies":
                        response = listMovies(intent);
                        break;
                    case "show_plot":
                        response = showPlot(intent);
                        break;
                }
            }
        } catch (IOException e) {
			e.printStackTrace();
		}


        CacheControl cc = new CacheControl();
        cc.setNoCache(true);
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        ResponseBuilder builder = Response.ok(objectMapper.writeValueAsString(response),"application/json");
        return builder.cacheControl(cc).build();
    }

    private AlexaIntentResponse showPlot(AlexaIntent intent){
        AlexaIntentResponse response = new AlexaIntentResponse();
        AlexaResponse alexaResp = new AlexaResponse();

        
        if(intent.getSlots() == null || intent.getSlots().get("Movie") == null){
            //No movie parameter
            AlexaOutputSpeech speech = new AlexaOutputSpeech();
            speech.setType("PlainText");
            speech.setPlayBehavior("ENQUEUE");    
            speech.setText("Please tell me the movie you want the plot.");
            AlexaReprompt reprompt = new AlexaReprompt();
            reprompt.setOutputSpeech(speech);
            alexaResp.setReprompt(reprompt);
        }else{
            Movie movie = movieService.getMovieDB(intent.getSlots().get("Movie").getValue());
            if(movie != null){
                AlexaOutputSpeech speech = new AlexaOutputSpeech();
                speech.setType("PlainText");
                speech.setPlayBehavior("ENQUEUE");
                speech.setText("This is the plot for '"+movie.getTitle()+"': "+movie.getPlot());
                alexaResp.setOutputSpeech(speech);
            }else{
                AlexaOutputSpeech speech = new AlexaOutputSpeech();
                speech.setType("PlainText");
                speech.setPlayBehavior("ENQUEUE");
                speech.setText("Sorry, couldn't find the movie.");
                alexaResp.setOutputSpeech(speech);
            }
        }
        
        response.setResponse(alexaResp);
        response.setVersion("1.0");
        return response;
    }

    private AlexaIntentResponse listMovies(AlexaIntent intent){
        AlexaIntentResponse response = new AlexaIntentResponse();

        List<Movie> movieList = movieService.listMoviesDB();
        AlexaResponse alexaResponse = new AlexaResponse();
        AlexaOutputSpeech outputSpeech = new AlexaOutputSpeech();
        StringBuilder sb = new StringBuilder();
        sb.append("Found these movies: ");
        for(Movie movie:movieList){
            sb.append(movie.getTitle());
            sb.append(", ");
        }
        outputSpeech.setText(sb.toString());
        outputSpeech.setType("PlainText");
        outputSpeech.setPlayBehavior("ENQUEUE");
        alexaResponse.setOutputSpeech(outputSpeech);
        response.setResponse(alexaResponse);
        response.setVersion("1.0");

        return response;
    }

}