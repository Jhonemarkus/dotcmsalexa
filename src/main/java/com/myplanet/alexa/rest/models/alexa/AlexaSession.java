package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaSession{
    @JsonProperty("new")
    boolean newSesssion;
    String sessionId;
    AlexaApplication application;
    AlexaUser user;

    public boolean isNewSesssion() {
        return this.newSesssion;
    }

    public void setNewSesssion(boolean newSesssion) {
        this.newSesssion = newSesssion;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public AlexaApplication getApplication() {
        return this.application;
    }

    public void setApplication(AlexaApplication application) {
        this.application = application;
    }

    public AlexaUser getUser() {
        return this.user;
    }

    public void setUser(AlexaUser user) {
        this.user = user;
    }
}