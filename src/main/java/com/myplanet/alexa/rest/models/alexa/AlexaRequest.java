package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaRequest{
    String type;
    String requestId;
    String timestamp;
    String locale;
    AlexaIntent intent;
    String reason;
    AlexaError error;

    public AlexaError getError() {
        return this.error;
    }

    public void setError(AlexaError error) {
        this.error = error;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public AlexaIntent getIntent() {
        return this.intent;
    }

    public void setIntent(AlexaIntent intent) {
        this.intent = intent;
    }
}