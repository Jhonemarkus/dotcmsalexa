package com.myplanet.alexa.rest.models;

import java.util.Map;

public class DialogFlowOutputContext {
    String name;
    Integer lifespanCount;
    Map<String,Object> parameters;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLifespanCount() {
        return this.lifespanCount;
    }

    public void setLifespanCount(Integer lifespanCount) {
        this.lifespanCount = lifespanCount;
    }

    public Map<String,Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String,Object> parameters) {
        this.parameters = parameters;
    }
}