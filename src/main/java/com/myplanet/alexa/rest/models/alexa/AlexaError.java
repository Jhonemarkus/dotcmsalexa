package com.myplanet.alexa.rest.models.alexa;

public class AlexaError{
    String type;
    String message;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}