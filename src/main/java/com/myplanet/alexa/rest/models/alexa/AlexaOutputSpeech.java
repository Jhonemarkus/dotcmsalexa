package com.myplanet.alexa.rest.models.alexa;

public class AlexaOutputSpeech{
    String type;
    String text;
    String ssml;
    String playBehavior;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSsml() {
        return this.ssml;
    }

    public void setSsml(String ssml) {
        this.ssml = ssml;
    }

    public String getPlayBehavior() {
        return this.playBehavior;
    }

    public void setPlayBehavior(String playBehavior) {
        this.playBehavior = playBehavior;
    }
}