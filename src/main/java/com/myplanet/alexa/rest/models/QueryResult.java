package com.myplanet.alexa.rest.models;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class QueryResult{
    String queryText;
    Map<String,Object> parameters;
    Boolean allRequiredParamsPresent;
    String fulfillmentText;
    List<JsonNode> fulfillmentMessages;
    List<DialogFlowOutputContext> outputContexts;
    Intent intent;
    Double intentDetectionConfidence;
    String languageCode;

    public String getQueryText() {
        return this.queryText;
    }

    public void setQueryText(String queryText) {
        this.queryText = queryText;
    }

    public Map<String,Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String,Object> parameters) {
        this.parameters = parameters;
    }

    public Boolean getAllRequiredParamsPresent() {
        return this.allRequiredParamsPresent;
    }

    public void isAllRequiredParamsPresent(Boolean allRequiredParamsPresent) {
        this.allRequiredParamsPresent = allRequiredParamsPresent;
    }

    public String getFulfillmentText() {
        return this.fulfillmentText;
    }

    public void setFulfillmentText(String fulfillmentText) {
        this.fulfillmentText = fulfillmentText;
    }

    public List<JsonNode> getFulfillmentMessages() {
        return this.fulfillmentMessages;
    }

    public void setFulfillmentMessages(List<JsonNode> fulfillmentMessages) {
        this.fulfillmentMessages = fulfillmentMessages;
    }

    public List<DialogFlowOutputContext> getOutputContexts() {
        return this.outputContexts;
    }

    public void setOutputContexts(List<DialogFlowOutputContext> outputContexts) {
        this.outputContexts = outputContexts;
    }

    public Intent getIntent() {
        return this.intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public Double getIntentDetectionConfidence() {
        return this.intentDetectionConfidence;
    }

    public void setIntentDetectionConfidence(Double intentDetectionConfidence) {
        this.intentDetectionConfidence = intentDetectionConfidence;
    }

    public String getLanguageCode() {
        return this.languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}