package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaResponse{
    AlexaOutputSpeech outputSpeech;
    AlexaCard card;
    AlexaReprompt reprompt;
    //Directives
    boolean shouldEndSession;

    public AlexaOutputSpeech getOutputSpeech() {
        return this.outputSpeech;
    }

    public void setOutputSpeech(AlexaOutputSpeech outputSpeech) {
        this.outputSpeech = outputSpeech;
    }

    public AlexaCard getCard() {
        return this.card;
    }

    public void setCard(AlexaCard card) {
        this.card = card;
    }

    public AlexaReprompt getReprompt() {
        return this.reprompt;
    }

    public void setReprompt(AlexaReprompt reprompt) {
        this.reprompt = reprompt;
    }

    public boolean isShouldEndSession() {
        return this.shouldEndSession;
    }

    public void setShouldEndSession(boolean shouldEndSession) {
        this.shouldEndSession = shouldEndSession;
    }
}