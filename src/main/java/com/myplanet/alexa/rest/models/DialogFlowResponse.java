package com.myplanet.alexa.rest.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DialogFlowResponse{
    String fulfillmentText;
    DialogFlowEventInput followupEventInput;
    List<DialogFlowOutputContext> outputContexts;

    public List<DialogFlowOutputContext> getOutputContexts() {
        return this.outputContexts;
    }

    public void setOutputContexts(List<DialogFlowOutputContext> outputContexts) {
        this.outputContexts = outputContexts;
    }

    public DialogFlowEventInput getFollowupEventInput() {
        return this.followupEventInput;
    }

    public void setFollowupEventInput(DialogFlowEventInput followupEventInput) {
        this.followupEventInput = followupEventInput;
    }

    public String getFulfillmentText() {
        return this.fulfillmentText;
    }

    public void setFulfillmentText(String fulfillmentText) {
        this.fulfillmentText = fulfillmentText;
    }
    
}