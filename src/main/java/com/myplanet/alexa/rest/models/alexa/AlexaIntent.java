package com.myplanet.alexa.rest.models.alexa;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaIntent{
    String name;
    String confirmationStatus;
    HashMap<String,AlexaSlot> slots;

    public HashMap<String,AlexaSlot> getSlots() {
        return this.slots;
    }

    public void setSlots(HashMap<String,AlexaSlot> slots) {
        this.slots = slots;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConfirmationStatus() {
        return this.confirmationStatus;
    }

    public void setConfirmationStatus(String confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }
}