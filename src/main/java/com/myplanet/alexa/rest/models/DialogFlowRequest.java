package com.myplanet.alexa.rest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DialogFlowRequest{

    String responseId;
    QueryResult queryResult;

  public String getResponseId() {
    return this.responseId;
  }

  public void setResponseId(String responseId) {
    this.responseId = responseId;
  }

  public QueryResult getQueryResult() {
    return this.queryResult;
  }

  public void setQueryResult(QueryResult queryResult) {
    this.queryResult = queryResult;
  }
    
}