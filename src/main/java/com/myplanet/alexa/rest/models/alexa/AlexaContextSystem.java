package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaContextSystem{
    AlexaApplication application;
    AlexaUser user;
    AlexaDevice device;
    String apiEndpoint;
    String apiAccessToken;

    public AlexaApplication getApplication() {
        return this.application;
    }

    public void setApplication(AlexaApplication application) {
        this.application = application;
    }

    public AlexaUser getUser() {
        return this.user;
    }

    public void setUser(AlexaUser user) {
        this.user = user;
    }

    public AlexaDevice getDevice() {
        return this.device;
    }

    public void setDevice(AlexaDevice device) {
        this.device = device;
    }

    public String getApiEndpoint() {
        return this.apiEndpoint;
    }

    public void setApiEndpoint(String apiEndpoint) {
        this.apiEndpoint = apiEndpoint;
    }

    public String getApiAccessToken() {
        return this.apiAccessToken;
    }

    public void setApiAccessToken(String apiAccessToken) {
        this.apiAccessToken = apiAccessToken;
    }
}