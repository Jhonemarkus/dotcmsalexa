package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaIntentRequest{
    String version;
    AlexaSession session;
    AlexaContext context;
    AlexaRequest request;

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AlexaSession getSession() {
        return this.session;
    }

    public void setSession(AlexaSession session) {
        this.session = session;
    }

    public AlexaContext getContext() {
        return this.context;
    }

    public void setContext(AlexaContext context) {
        this.context = context;
    }

    public AlexaRequest getRequest() {
        return this.request;
    }

    public void setRequest(AlexaRequest request) {
        this.request = request;
    }
}
