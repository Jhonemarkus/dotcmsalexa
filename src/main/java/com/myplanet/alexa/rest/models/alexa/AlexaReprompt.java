package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaReprompt{
    AlexaOutputSpeech outputSpeech;

    public AlexaOutputSpeech getOutputSpeech() {
        return this.outputSpeech;
    }

    public void setOutputSpeech(AlexaOutputSpeech outputSpeech) {
        this.outputSpeech = outputSpeech;
    }
}