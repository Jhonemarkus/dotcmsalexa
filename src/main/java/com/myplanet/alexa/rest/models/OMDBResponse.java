package com.myplanet.alexa.rest.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class OMDBResponse {

	private boolean response;
	private String error;
	private List<Movie> search;
	
	public boolean isResponse() {
		return response;
	}
	public void setResponse(boolean response) {
		this.response = response;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public List<Movie> getSearch() {
		return search;
	}
	public void setSearch(List<Movie> search) {
		this.search = search;
	}
	
}
