package com.myplanet.alexa.rest.models.alexa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaContext{
    AlexaContextSystem System;

    public AlexaContextSystem getSystem() {
        return this.System;
    }

    public void setSystem(AlexaContextSystem System) {
        this.System = System;
    }
}