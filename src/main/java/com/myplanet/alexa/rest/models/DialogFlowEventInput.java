package com.myplanet.alexa.rest.models;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DialogFlowEventInput{
    String name;
    String languageCode;
    HashMap<String,Object> parameters;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguageCode() {
        return this.languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public HashMap<String,Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(HashMap<String,Object> parameters) {
        this.parameters = parameters;
    }
}