package com.myplanet.alexa.rest.models.alexa;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AlexaIntentResponse{
    String version;
    HashMap<String,Object> sessionAttributes;
    AlexaResponse response;

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public HashMap<String,Object> getSessionAttributes() {
        return this.sessionAttributes;
    }

    public void setSessionAttributes(HashMap<String,Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }

    public AlexaResponse getResponse() {
        return this.response;
    }

    public void setResponse(AlexaResponse response) {
        this.response = response;
    }
}