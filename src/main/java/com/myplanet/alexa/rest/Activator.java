package com.myplanet.alexa.rest;

import org.osgi.framework.BundleContext;
import com.dotcms.rest.config.RestServiceUtil;
import com.dotmarketing.osgi.GenericBundleActivator;
import com.dotmarketing.util.Logger;

public class Activator extends GenericBundleActivator {

	Class importEndpoint = AlexaEndpoint.class;
	Class readEndpoint = ReadMovieEndpoint.class;

	public void start(BundleContext context) throws Exception {

		Logger.info(this.getClass(), "Adding new Restful Service:" + importEndpoint.getSimpleName()+", "+readEndpoint.getSimpleName());
		RestServiceUtil.addResource(importEndpoint);
		RestServiceUtil.addResource(readEndpoint);
	}

	public void stop(BundleContext context) throws Exception {

		Logger.info(this.getClass(), "Removing new Restful Service:" + importEndpoint.getSimpleName()+", "+readEndpoint.getSimpleName());
		RestServiceUtil.removeResource(importEndpoint);
		RestServiceUtil.removeResource(readEndpoint);

	}

}